# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"

        score = successorGameState.getScore()

        index = 0
        dist_to_food = []
        for i in newFood.asList():
            dist_to_food.append(manhattanDistance(newPos, i))
            index = index + 1
        if len(dist_to_food) != 0:
            closest_food = min(dist_to_food)
            score += 12.0/closest_food

        dist_to_ghost = manhattanDistance(newPos, newGhostStates[0].getPosition())
        if dist_to_ghost > 0:
            score -= 12.0/dist_to_ghost

        return score

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game
        """
        "*** YOUR CODE HERE ***"
        
        return self.findMax(gameState, 1)[1]

    def findMax(self, gameState, depth):
        possible_actions = gameState.getLegalActions(0)

        #bottom nodes:
        if depth > self.depth or gameState.isWin() or len(possible_actions)==0:
                return (self.evaluationFunction(gameState), None)

        minValues_and_actions = []
        for i in possible_actions:
            successor = gameState.generateSuccessor(0, i)
            minValues_and_actions.append((self.findMin(successor, 1, depth), i))

        return max(minValues_and_actions)

    def findMin(self, gameState, agent, depth):
        ghost_actions = gameState.getLegalActions(agent)

        if len(ghost_actions) == 0 or gameState.isLose() or gameState.isWin():
            return (self.evaluationFunction(gameState), None)

        successors = []
        if agent + 1 == gameState.getNumAgents():
            for i in ghost_actions:
                successor = gameState.generateSuccessor(agent, i)
                successors.append(self.findMax(successor, depth+1))
            return min(successors)
        else:
            for i in ghost_actions:
                successor = gameState.generateSuccessor(agent, i)
                successors.append(self.findMin(successor, agent+1, depth))
            return min(successors)

 

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        return self.findMax(gameState, 1, -999999.0, 999999.0)[1]


    def findMax(self, gameState, depth, alpha, beta):
        max_value = -999999.0
        desired_action = Directions.STOP

        possible_actions = gameState.getLegalActions(0)

        if depth > self.depth or gameState.isWin() or len(possible_actions)==0:
            return (self.evaluationFunction(gameState), Directions.STOP)

        for i in possible_actions:
            successor = gameState.generateSuccessor(0,i)
            new_value = self.findMin(successor, 1, depth, alpha, beta)[0]

            if new_value > max_value:
                max_value = new_value
                desired_action = i

            if max_value > beta:
                return (max_value, desired_action)

            alpha = max(alpha, max_value)

        return (max_value, desired_action)

    def findMin(self, gameState, agent, depth, alpha, beta):
        min_value = 999999.0
        desired_action = Directions.STOP

        ghost_actions = gameState.getLegalActions(agent)

        if len(ghost_actions) == 0 or gameState.isLose() or gameState.isWin():
            return (self.evaluationFunction(gameState), Directions.STOP)

        if agent + 1 == gameState.getNumAgents():
            for i in ghost_actions:
                successor = gameState.generateSuccessor(agent, i)
                new_value = self.findMax(successor, depth+1, alpha, beta)[0]
                if new_value < min_value:
                    min_value = new_value
                    desired_action = i
                if min_value < alpha:
                    return (min_value, desired_action)
                beta = min(beta, min_value)
        else:
            for i in ghost_actions:
                successor = gameState.generateSuccessor(agent, i)
                new_value = self.findMin(successor, agent+1, depth, alpha, beta)[0]
                if new_value < min_value:
                    min_value = new_value
                    desired_action = i
                if min_value < alpha:
                    return (min_value, desired_action)
                beta = min(beta, min_value)

        return (min_value, desired_action)

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def findMax(self, gameState, depth):
        possible_actions = gameState.getLegalActions(0)

        if len(possible_actions) == 0 or depth>self.depth or gameState.isWin() or gameState.isLose():
            return (self.evaluationFunction(gameState), None)

        expectedValues_and_actions = []
        for i in possible_actions:
            successor = gameState.generateSuccessor(0, i)
            max_expected_score = self.getMaxExpectedScore(successor, 1, depth)[0]
            expectedValues_and_actions.append((max_expected_score, i))

        return max(expectedValues_and_actions)

    def getMaxExpectedScore(self, gameState, agent, depth):
        possible_actions = gameState.getLegalActions(agent)

        if len(possible_actions) == 0 or depth>self.depth or gameState.isWin() or gameState.isLose():
            return (self.evaluationFunction(gameState), None)

        expected_values = []
        successor_states = []
        for i in possible_actions:
            successor_states.append(gameState.generateSuccessor(agent, i))

        for i in successor_states:
            if agent+1 == gameState.getNumAgents():
                expected_values.append(self.findMax(i, depth+1))
            else:
                expected_values.append(self.getMaxExpectedScore(i, agent+1, depth))

        average_value = 0;
        for i in expected_values:
            average_value += float(i[0])
        average_value = average_value / len(expected_values)

        return (average_value, None)
 

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        return self.findMax(gameState,1)[1]

def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    # Useful information you can extract from a GameState (pacman.py)
    #successorGameState = currentGameState.generatePacmanSuccessor(action)
    newPos = currentGameState.getPacmanPosition()
    newFood = currentGameState.getFood()
    newGhostStates = currentGameState.getGhostStates()
    newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

    score = currentGameState.getScore()

    food_weight = 11.0
    ghost_weight = 3.0

    index = 0
    dist_to_food = []
    dist_to_ghost = 50.0
    for i in newFood.asList():
        dist_to_food.append(manhattanDistance(newPos, i))
        index = index + 1
    if len(dist_to_food) != 0:
        closest_food = min(dist_to_food)
        score += food_weight/closest_food

        dist_to_ghost = manhattanDistance(newPos, newGhostStates[0].getPosition())
    if dist_to_ghost > 0:
        score -= ghost_weight/dist_to_ghost

    return score



# Abbreviation
better = betterEvaluationFunction

